Errors & Exceptions
-------------------

WowApiError
^^^^^^^^^^^

In case a request to the api goes wrong or any other failure, an ``WowApiError``
will be raised.


WowClientError
^^^^^^^^^^^^^^

In case the client fails on http or networking, a ``WowClientError`` will be raised.
