Changelog
=========

0.4 (09-07-2014)
----------------
* Fix importing requests module on setup before the requirement could be satisfied.
* Update requirements.

0.3 (27-07-2014)
----------------
* test with pytest.
* update docs.

0.2 (27-09-2013)
----------------

* Fix PEP8 Style.
* Remove ``all`` property from AuctionResouce.
* Add fetch argument to ``is_new`` method of AuctionResource.
* Add ``download_auctions`` method to AuctionResource.
* Update documentation.
* Update tests.

0.1 (02-06-2013)
----------------

* Initial release.