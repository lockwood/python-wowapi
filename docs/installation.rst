Installation
============

Here you can read how to install the package.


Pip
---

* Install the package with ``pip`` in your terminal::

    pip install python-wowapi


* Or install the package directly from Bitbucket::

    pip install git+https://bitbucket.org/lockwood/python-wowapi.git


Running tests
-------------

To run the tests, you need to install the test requirements with pip:

.. code-block:: bash

    $ pip install -e .[tests]


Now you can run the tests from the root folder of the package:

.. code-block:: bash

    $ make tests
